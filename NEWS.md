
# ipkg 1.1.2

### Major change

* The alternative proxy site is changed from <https://mirror.ghproxy.com/> to <https://ghfast.top/>.
* proxy website <https://gh-proxy.com/> changed from second to first.

# ipkg 1.1.1

### Major change

* The alternative proxy site is changed from <https://ghproxy.com/> to <https://mirror.ghproxy.com/>.

# ipkg 1.1.0

### Major change

* `download_file()` is enhanced to download file from non-Github sites.

# ipkg 1.0.9

### Major change

* Add a alternative proxy site <https://gh-proxy.com/>.
* Add a new function `conn_test()` tests whether the website is properly connected.

# ipkg 1.0.8

### Fix Bug

- Change title field in title case.

# ipkg 1.0.7

### Major change

* Add a new function `download_file()`, which download file from GitHub via the proxy site <https://ghproxy.com/>.

# ipkg 1.0.6

### Fix Bug

* Install R packages on GitHub via the proxy site <https://ghproxy.com/>.

# ipkg 1.0.5

### Fix Bug

* Change <https://hub.fastgit.org/> to <https://hub.fastgit.xyz/>.

# ipkg 1.0.4

### Fix Bug

* Change BugReports field value to ".../-/issues".

### Major change

* Change the parameters of install_github from x to pkg.

# ipkg 1.0.3

### Fix Bug

* Add trailing slashes.
* Delete 'VignetteBuilder' field.
* Delete test file.
* Change title field in title case.
* Do not execute the example code.

# ipkg 1.0.2

### Major change

* Change the function name ig to install_github.

# ipkg 1.0.1

### Fix Bug

* Add test file.
* Fix GitHub case.

# ipkg 1.0.0

### New Features

* First public release.
